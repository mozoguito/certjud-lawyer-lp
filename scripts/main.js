var app = new Vue({
  el: '#app',

  data: {
    infoBlocks:[
      {
        title: 'Quem Somos',
        text: 'A Certjud é uma empresa especializada em cessão de créditos judiciais e antecipação de honorários advocatícios.',
        img: './img/icons/aboutUs.svg'
      }, 
      {
        title: 'Cessão de Créditos',
        text: 'Deixe seu cliente satisfeito! Compramos o processo e realizamos o pagamento em até 48 horas.',
        img: './img/icons/coins.svg'
      },
      {
        title: 'Advogado Parceiro',
        text: 'Você não precisa mais esperar os seus honorários. Realizamos a antecipação',
        img: 'https://pngimage.net/wp-content/uploads/2018/06/partner-icon-png-4.png',
      }
    ],
    name: "",
    email: "",
    tel: "",

    filled: false
  },

  methods: {
    tiggerMsg(){
      if(!this.filled){
        Array.of('name', 'email', 'tel').map(key => {
          if(this[key]) this.filled = true
          else this.filled = false
        })
      }
    }
  }
})